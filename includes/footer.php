	
	<!-- FOOTER-START -->
	
	<footer class="e_main_footer">
	</footer>
	
	<!-- INCLUDED-JAVASCRIPTS -->
	
	<script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/material.min.js"></script>
	<script src="../assets/js/custom.js"></script>
	<script src="https://use.fontawesome.com/b0a450f77f.js"></script>
	
	<!-- END-INCLUDED-JAVASCRIPTS -->
	
</body>
</html>

	<!-- END-FOOTER -->