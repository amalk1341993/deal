
	<!-- HEADER-START -->

<!DOCTYPE html>

<html lang="en">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../assets/img/e_logo_nav_bar.png">

	<!-- WEB-TITLE -->

		<title>E Office Portal</title>

    <!-- INCLUDED-CASCADE-STYLE-SHEETS -->

		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/custom.css" rel="stylesheet">
		<link href="../assets/css/theme.css" rel="stylesheet">
		<link href="../assets/css/material.min.css" rel="stylesheet">
		<link href="../assets/css/font-awesome.min.css" rel="stylesheet">
	</head>

	<body class="widthFull heightFull">

	<!-- MAIN-NAVIGATION-BAR -->

		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top h_main_navbar">
			<a class="navbar-brand" href="#">Navbar</a>
	  
	<!-- RESPONSIVE-COLLAPSE -->
	
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
	 
	<!-- END-RESPONSIVE-COLLAPSE -->
	 
	<!-- COLLAPSE-NAV-BAR -->
	
			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
				  <li class="nav-item active">
					<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" href="#">Link</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link disabled" href="#">Disabled</a>
				  </li>
				  <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu" aria-labelledby="dropdown01">
					  <a class="dropdown-item" href="#">Action</a>
					  <a class="dropdown-item" href="#">Another action</a>
					  <a class="dropdown-item" href="#">Something else here</a>
					</div>
				  </li>
				</ul>
			</div>
	  
	<!-- ENDS-COLLAPSE-NAV-BAR -->
	  
		</nav>

	<!-- END-MAIN-NAVIGATION-BAR -->

	<!-- END-HEADER -->
