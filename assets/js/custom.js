
/*
  Site Name: E-Office Portal - Online office management system
  URI: http:http://eoffice.techware.co.in/
  Description: Customized JS for E-Office Portal
  Version: 2.0
  Author: Amal-Techware Solution
  Author URI: 
  Tags:
  
  ---------------------------
  CUSTOM CONPONENT-SCRIPTS
  ---------------------------
  
  TABLE OF CONTENTS
  ---------------------------
   01. FONTS-N-SIZES 
   02. CURRENT-TIME



*/

$(function(){


/*-------------------------------

01. HEADER-SHRINKER

--------------------------------*/

    $(window).scroll(function() {
  if ($(document).scrollTop() > 52) {
    $('.h_main_navbar').addClass('h_main_navbar_shrink');
  } else{
    $('.h_main_navbar').removeClass('h_main_navbar_shrink');
  }
});




});

